const Pusher = require("pusher");

const pusher = new Pusher({
  appId: "1200890",
  key: "ae4ed345442d07569de4",
  secret: "97018a52122abf8ecdcb",
  cluster: "eu",
  useTLS: true
});

var osc = require('node-osc');
var io = require('socket.io')(8081);


var oscServer, oscClient;

var isConnected = false;

let delay = 2000
let wait = false

io.sockets.on('connection', function (socket) {
	console.log('connection');
	// socket.on("config", function (obj) {
	// 	console.log('config: ', obj)
	// 	isConnected = true;
    	oscServer = new osc.Server(8338, '127.0.0.1');
	    oscClient = new osc.Client('127.0.0.1', 3334);
	    oscClient.send('/status', socket.sessionId + ' connected');
		oscServer.on('message', function(msg, rinfo) {
			// good message
			if(msg[0] !== '#bundle' 
			|| !msg[2].length 
			|| msg[2][0] !== '/found' 
			|| msg[2][1] !== 1) {
				return;
			}


			if(!wait) {
				wait = true
				setTimeout(() => {
					console.log('oscServer message: ')

					pusher.trigger("my-channel", "my-event", msg);
					wait = false
				}, delay);
			}
			socket.emit("message", msg);

		});
		// socket.emit("connected", 1);
	// });
 	socket.on("message", function (obj) {
		 console.log('on message: ')

		// pusher.trigger("my-channel", "my-event", {
		// 	message: "hello world"
		// });

		// oscClient.send.apply(oscClient, obj);
  	});
	socket.on('disconnect', function(){
		console.log('disconnect: ', isConnected)
		if (isConnected) {
			oscServer.kill();
			oscClient.kill();
		}
  	});
});